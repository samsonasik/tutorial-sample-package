<?php

declare(strict_types=1);

namespace TutorialSamplePackage;

use function sprintf;

final class HelloWorld
{
    public static function message(string $message) : void
    {
        echo sprintf('Hello to world: %s', $message);
    }
}
